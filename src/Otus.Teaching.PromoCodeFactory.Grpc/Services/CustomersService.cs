using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace GrpcServer.Services
{
    public class CustomersService : Customers.CustomersBase
    {


        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<IdResponse> Create(CreateCustomerRequest request, ServerCallContext context)
        {

            var id = Guid.NewGuid();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(p=>Guid.Parse(p)).ToList());

            var customer = new Customer()
            {
                Id = id,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = preferences.Select(p => new CustomerPreference() { PreferenceId = p.Id, CustomerId = id}).ToList()
            };

            await _customerRepository.AddAsync(customer);

            return new IdResponse() { Id = id.ToString() }
;        }

        public override async Task<Empty> Edit(EditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return new Empty();

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(p => Guid.Parse(p)).ToList());
            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Preferences = preferences.Select(p => new CustomerPreference() { PreferenceId = p.Id, CustomerId = customer.Id }).ToList();
           
            await _customerRepository.UpdateAsync(customer);

            return new Empty();
        }

        public override async Task<Empty> Delete(IdRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return new Empty();

            await _customerRepository.DeleteAsync(customer);
            return new Empty();
        }
           

        public override async Task<GetAllResponse> GetAll(Empty request, ServerCallContext context)
        {
            var response = new GetAllResponse();
            var customers = await _customerRepository.GetAllAsync();
            response.List.AddRange(customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id.ToString(),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }));
            return response;

        }


        public override async Task<CustomerResponse> GetById(IdRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return null;

            return new CustomerResponse() { Id = customer.Id.ToString(), Email = customer.Email, FirstName = customer.FirstName, LastName =customer.LastName };
        }

      
    }
}